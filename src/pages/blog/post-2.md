--- 
layout: '../../layouts/PostLayout.astro'
title: Excepteur Sint
date: 2023-02-22
description: Pellentesque habitant morbi tristique
draft: false
category: Tech
image: {
    src: '/images/plasma_ball.jpg',
    alt: 'plasma_ball'   
}
---

EXCEPTEUR SINT

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt dui ut ornare lectus sit amet est. Consectetur a erat nam at lectus urna duis. Risus at ultrices mi tempus imperdiet nulla malesuada. Sed enim ut sem viverra aliquet. Sed vulputate mi sit amet mauris commodo quis imperdiet massa. Laoreet id donec ultrices tincidunt arcu non sodales neque. Iaculis eu non diam phasellus vestibulum lorem sed risus ultricies. Suscipit tellus mauris a diam maecenas. A scelerisque purus semper eget duis. Ultrices mi tempus imperdiet nulla malesuada.

<br>

Vel pretium lectus quam id leo in vitae turpis. Ac turpis egestas maecenas pharetra convallis. Ullamcorper sit amet risus nullam eget felis. At auctor urna nunc id cursus. Ornare quam viverra orci sagittis. Sit amet nulla facilisi morbi tempus iaculis. Eu augue ut lectus arcu bibendum. Amet nulla facilisi morbi tempus iaculis urna id. Vitae sapien pellentesque habitant morbi tristique senectus. Augue interdum velit euismod in pellentesque. Tristique sollicitudin nibh sit amet commodo nulla. Nibh ipsum consequat nisl vel. Felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Lorem dolor sed viverra ipsum nunc aliquet. Ligula ullamcorper malesuada proin libero nunc.