--- 
layout: '../../layouts/PostLayout.astro'
title: Ornare Quam
date: 2023-02-22
description: Aenean pharetra magna ac placerat vestibulum
draft: false
category: Tech
image: {
    src: '/images/digital_neon_display.jpg',
    alt: 'digital_neon_display'   
}
---

ORNARE QUAM

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus. Eget mi proin sed libero enim sed. Faucibus turpis in eu mi bibendum. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Quam vulputate dignissim suspendisse in est ante in. Aenean pharetra magna ac placerat vestibulum lectus mauris ultrices eros. Lobortis elementum nibh tellus molestie nunc. Ut eu sem integer vitae justo eget magna. 

<br>

Elit ut aliquam purus sit amet. Et tortor consequat id porta nibh venenatis. Pellentesque habitant morbi tristique senectus et netus. Sollicitudin nibh sit amet commodo nulla facilisi nullam. Justo nec ultrices dui sapien eget mi proin sed libero. Integer quis auctor elit sed. Placerat vestibulum lectus mauris ultrices. Massa tincidunt dui ut ornare lectus sit amet est. Non consectetur a erat nam at lectus. A arcu cursus vitae congue mauris rhoncus aenean. In hac habitasse platea dictumst quisque sagittis purus sit amet.