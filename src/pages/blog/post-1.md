--- 
layout: '../../layouts/PostLayout.astro'
title: Duis Aute
date: 2023-02-22
description: A arcu cursus vitae congue mauris rhoncus aenean
draft: false
category: Nature
image: {
    src: '/images/tiger_shark.jpg',
    alt: 'tiger_shark'   
}
---

DUIS AUTE

   Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta consectetur corporis officia dolor nisi architecto, quod, porro inventore quos perspiciatis sint pariatur qui culpa, sequi nemo deleniti deserunt voluptatibus. Deleniti provident nesciunt voluptates excepturi molestiae illo ad sit tenetur, corporis, eligendi ipsam maxime blanditiis non corrupti temporibus vel sunt omnis aperiam quaerat quidem illum? Amet asperiores, itaque id reiciendis voluptates rem. Sit, fugit accusantium asperiores nemo obcaecati blanditiis! Facere fugiat accusantium asperiores modi ducimus voluptates sapiente culpa aliquam laboriosam. Minus veritatis voluptas soluta veniam pariatur consequatur amet et quaerat doloribus. Sed magni possimus pariatur temporibus obcaecati dolorum? Explicabo, accusamus itaque!

   <br>

 Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta consectetur corporis officia dolor nisi architecto, quod, porro inventore quos perspiciatis sint pariatur qui culpa, sequi nemo deleniti deserunt voluptatibus. Deleniti provident nesciunt voluptates excepturi molestiae illo ad sit tenetur, corporis, eligendi ipsam maxime blanditiis non corrupti temporibus vel sunt omnis aperiam quaerat quidem illum? Amet asperiores, itaque id reiciendis voluptates rem. Sit, fugit accusantium asperiores nemo obcaecati blanditiis! Facere fugiat accusantium asperiores modi ducimus voluptates sapiente culpa aliquam laboriosam. Minus veritatis voluptas soluta veniam pariatur consequatur amet et quaerat doloribus. Sed magni possimus pariatur temporibus obcaecati dolorum? Explicabo, accusamus itaque!