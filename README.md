# My Astro Blog

My new blog project using Astro and local markdown files for content. Features sorting by category and persistant light/dark mode switcher and adjusts to your system mode preferences. Big thanks to [Coding in Public](https://github.com/coding-in-public) for his tutorial and codebase, I started with it and added custom styling and a little added functionality.